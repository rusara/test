FROM openjdk:11

COPY . /usr/src/
WORKDIR /usr/src/
RUN javac test.java
CMD ["java", "test"]